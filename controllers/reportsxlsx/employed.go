package reportsxlsx

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/riyan/apiatex/controllers/dbaccess"
	"github.com/riyan/apiatex/controllers/webserver/webhandler"
	"github.com/riyan/apiatex/models"
)

func employeWritedHandler(w http.ResponseWriter, r *http.Request) {
	date := time.Now()
	y, m, d := date.Date()
	filename := fmt.Sprintf("employed-%d-%s-%d.xlsx", d, m, y)
	w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s", filename))
	// w.Header().Set("Content-Disposition", "attachment")
	w.Header().Set("Content-Type", "application/vnd.ms-excel")
	db, err := webhandler.DBFromContext(r.Context())
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	var c dbaccess.Cursor
	buf := &bytes.Buffer{}
	if err = EmployedList(db, buf, c); err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	n, err := io.Copy(w, buf)
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	// n := buf.Len()
	w.Header().Set("Content-Length", fmt.Sprintf("%d", n))
}
func employeReadHandler(w http.ResponseWriter, r *http.Request) {
	db, err := webhandler.DBFromContext(r.Context())
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	employed, err := ReadEmploye(r.Body)
	if err != nil {
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	for _, v := range employed {
		err := v.Insert(db)
		if err != nil {
			fmt.Println("Error : ", err)
		}
	}
}

func ReadEmploye(r io.Reader) ([]models.Employed, error) {
	xlsx, err := excelize.OpenReader(r)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	// Get value from cell by given worksheet name and axis.
	// cell := xlsx.GetCellValue("Sheet1", "B2")
	// fmt.Println(cell)
	// Get all the rows in the Sheet1.
	employeds := []models.Employed{}
	rows := xlsx.GetRows("Sheet1")
	for _, row := range rows {
		e := models.Employed{
			Id:           row[0],
			NameEmployed: row[1],
			Email:        row[2],
			Phone:        row[3],
			Address:      row[4],
		}
		employeds = append(employeds, e)
		// for _, colCell := range row {
		// 	fmt.Print(colCell, "\t")
		// }
		// fmt.Println()
	}
	return employeds, nil

}

func EmployedList(db dbaccess.DBExecer, w io.Writer, c dbaccess.Cursor) error {
	sheet := "Employe"
	xlsx := excelize.NewFile()
	xlsx.SetSheetName("Sheet1", sheet)
	b, err := bold(xlsx, sheet)
	if err != nil {
		return err
	}
	xlsx.SetCellStyle(sheet, "A2", "E2", b)
	xlsx.SetColWidth(sheet, "B", "B", 40)
	xlsx.SetColWidth(sheet, "C", "C", 30)
	xlsx.SetColWidth(sheet, "E", "E", 80)
	xlsx.SetCellValue(sheet, "A2", "ID")
	xlsx.SetCellValue(sheet, "B2", "Nama Employe")
	xlsx.SetCellValue(sheet, "C2", "Email")
	xlsx.SetCellValue(sheet, "D2", "Phone")
	xlsx.SetCellValue(sheet, "E2", "Address")
	// xlsx.SetActiveSheet(idx)
	emp, _, err := models.Employeds(db, c)
	if err != nil {
		return err
	}
	row := 2
	for _, v := range emp {
		row += 1
		xlsx.SetCellValue(sheet, fmt.Sprintf("A%d", row), v.Id)
		xlsx.SetCellValue(sheet, fmt.Sprintf("B%d", row), v.NameEmployed)
		xlsx.SetCellValue(sheet, fmt.Sprintf("C%d", row), v.Email)
		xlsx.SetCellValue(sheet, fmt.Sprintf("D%d", row), v.Phone)
		xlsx.SetCellValue(sheet, fmt.Sprintf("E%d", row), v.Address)
	}

	err = xlsx.Write(w)
	if err != nil {
		return err
	}
	// w.Header().Set("cont")
	fmt.Printf("write to the w err:%v\n employed:%v", err, emp)
	return err
}

func bold(xlsx *excelize.File, sheet string) (style int, err error) {
	style, err = xlsx.NewStyle(string(`{"font":{"bold":true,"size":16,"color":"#777777"},
    "fill":{"type":"pattern","color":["#E0EBF5"],"pattern":1}}`))
	if err != nil {
		return style, err
	}
	return style, err
}
