package reportsxlsx

import (
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/riyan/apiatex/controllers/webserver/webhandler"
)

type excelHandler struct {
	pattern string
}

func ExcelHandler(pattern string) http.Handler {
	xh := webhandler.New(excelHandler{pattern: pattern})
	return xh
}

func (xh excelHandler) Handle(w http.ResponseWriter, r *http.Request) webhandler.Response {
	res := webhandler.Response{Handled: true}
	path := strings.TrimPrefix(r.URL.Path, xh.pattern)
	paths := strings.Split(path, "/")
	url := paths[0]
	switch url {
	case "employed":
		fmt.Println("download employed as excel")
		employeWritedHandler(w, r)
	case "uploademployed":
		employeReadHandler(w, r)
		fmt.Println("download employed as excel")
	default:
		res.Error(errors.New("URL not valid"), http.StatusInternalServerError)
	}

	return res
}
